using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotDown : Switch
{
    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Hand")
        {
            isOn = false;
            ActionsController.OnShotDownMachine?.Invoke(true);
        }
    }
}
