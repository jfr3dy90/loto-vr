using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    [SerializeField] private CanvasGroup cg;
    [SerializeField] private GameObject[] uiScreen;
    [SerializeField] private TMP_Text title;
    [SerializeField] private TMP_Text msj;
    [SerializeField] private TMP_Text buttonText;
    [SerializeField] public Button btnAction;
    [SerializeField] private Image titleBG;
    private IEnumerator onUIBehaviour;

    [SerializeField] private Transform uiStepIndication;
    [SerializeField] private TMP_Text txtIndex;

    private int actualActionIndex;
    private bool needAction;
    
    private void Awake()
    {
        ActionsController.OnSetUi += SetScreen;
        ActionsController.OnSetIndicator += OnStartSetIndicator;
        btnAction.onClick.AddListener(OnButtonAction);
    }

    public void SetScreen(int type, int index, string _title, string _msj, string _buttonText, bool _needAction, int _indexAction, Vector2 sd)
    {
        needAction = _needAction;
        actualActionIndex = _indexAction;
        titleBG.rectTransform.sizeDelta = sd;
        switch (type)
        {
            case 0:
                btnAction.gameObject.SetActive(false);
                break;
            case 1:
                btnAction.gameObject.SetActive(true);
                break;
        }
        
        
        OnStartUiBehaviour(0, 1, false, 0);

        if (_title != null)
        {
            title.text = _title;
        }

        if (_msj != null)
        {
            msj.text = _msj;
        }

        if (_buttonText != null)
        {
            buttonText.text = _buttonText;
        }


        for (int i = 0; i < uiScreen.Length; i++)
        {
           uiScreen[i].SetActive(i == index);
        }
    }

    void OnStartUiBehaviour(int start, int finish, bool callAction, int indexAction)
    {
        if (onUIBehaviour != null)
        {
            StopCoroutine(onUIBehaviour);
        }

        onUIBehaviour = OnUiBehaviour(start, finish, callAction, indexAction);
        StartCoroutine(onUIBehaviour);
    }

    public IEnumerator OnUiBehaviour(int star, int finish, bool callAction, int indexAction)
    {
        cg.alpha = star;
        while (cg.alpha != finish)
        {
            yield return null;
            cg.alpha = Mathf.MoveTowards(cg.alpha, finish, 0.5f * Time.deltaTime);
        }

        if (callAction)
        {
            OnFinishStep(indexAction);
        }
    }

    void OnFinishStep(int index)
    {
        switch (index)
        {
            case 0 :
                XperienceManager.Instance.SetXperienceStep(Step.Step_1);
                break;
            case 1 :
                XperienceManager.Instance.SetXperienceStep(Step.Step_2);
                break;
            case 2:
                XperienceManager.Instance.SetXperienceStep(Step.Step_3);
                break;
            case 3:
                XperienceManager.Instance.SetXperienceStep(Step.ApagadoMaquina);
                break;
            case 4:
                XperienceManager.Instance.SetXperienceStep(Step.ApplicateLock);
                break;
            case 5:
                XperienceManager.Instance.SetXperienceStep(Step.CheckEnergy);
                break;
            case 6:
                XperienceManager.Instance.SetXperienceStep(Step.StartMachine);
                break;
            case 7:
                XperienceManager.Instance.LoadSim();
                break;
        }
    }

    public void OnButtonAction()
    {
        OnStartUiBehaviour(1,0, needAction, actualActionIndex);
    }

    void OnStartSetIndicator(Vector3 desirePosition, Vector3 globalRot, string index, bool isActive)
    {
        uiStepIndication.gameObject.SetActive(isActive);
        uiStepIndication.position = desirePosition;
        uiStepIndication.localRotation = quaternion.Euler(globalRot);
        txtIndex.text = index;
    }
    
    private void OnDestroy()
    {
        ActionsController.OnSetUi -= SetScreen;
        ActionsController.OnSetIndicator -= OnStartSetIndicator;

    }
}
