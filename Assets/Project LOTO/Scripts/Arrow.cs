using System;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using UnityEngine;

public class Arrow : MonoBehaviour
{
  public static Arrow Instance;

  [SerializeField] private Transform tr;
  private IEnumerator moveArrow;
  public float timeToMove;

  private void Awake()
  {
    if (Instance == null)
    {
      Instance = this;
    }
    else
    {
      Destroy(gameObject);
    }
  }

  public void SetArrow(bool _active)
  {
    tr.gameObject.SetActive(_active); 
  }

  public void OnStartMovement(Vector3 startPosition, Vector3 hotPoint, Vector3 offset, float t)
  {
    if (moveArrow != null)
    {
      StopCoroutine(moveArrow);
    }

    moveArrow = Movement(startPosition, hotPoint, offset, t);
    StartCoroutine(moveArrow);
  }

  IEnumerator Movement(Vector3 startPosition, Vector3 hotPoint, Vector3 offset, float _t)
  {
    float currentTime = 0;
    bool isFinish = false;
    Vector3 a;
    Vector3 b;
    float t = 0;

    if (_t == 0)
    {
      tr.position = startPosition + offset;
      a = startPosition;
      b = hotPoint;
    }
    else
    {
      tr.position = hotPoint+offset;
      a = hotPoint;
      b = startPosition;
    }
    
    tr.LookAt(hotPoint);

    while (!isFinish)
    {
      if (Vector3.Distance(transform.position, b+offset) < 0.01f)
      {
        isFinish = true;
        break;
      }

      t = (currentTime / timeToMove);
      t = t * t * t * (t * (t * 6 - 15) + 10);
      transform.position = Vector3.Lerp(a+offset, b + offset, t);
      currentTime += Time.deltaTime;
      yield return null;
    }

    if (t > 0.9)
    {
      OnStartMovement(a, b, offset, 1f);
    }
    else
    {
      OnStartMovement(a, b, offset, 0f);
    }
  }
}
