using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class GrabbableController : MonoBehaviour
{
    public static GrabbableController Instance;
    
    [SerializeField]private XRInteractionManager _interactionManager;
    public MeshCollider[] m_grabbables;
    public XRSocketInteractor[] m_Socket;
    public GameObject m_toolBox;
    public MeshCollider coll;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
        _interactionManager = FindObjectOfType<XRInteractionManager>();
    }

    public void SetGrabbable(int index)
    {
        for (int i = 0; i < m_grabbables.Length; i++)
        {
            m_grabbables[i].enabled = i == index;
        }
    }

    public void SetXrSockets(int indexSocket)
    {
        _interactionManager.SelectEnter(m_Socket[indexSocket], (IXRSelectInteractable)m_Socket[indexSocket].startingSelectedInteractable);
    }

    public void SetToolBox(bool isActive)
    {
        m_toolBox.SetActive(isActive);
    }

    public void DidableGrababbles()
    {
        for (int i = 0; i < m_grabbables.Length; i++)
        {
            m_grabbables[i].gameObject.SetActive(false);            
        }
    }


}