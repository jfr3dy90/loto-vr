using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class HeadController : MonoBehaviour
{
    [SerializeField] private Transform target;
    public bool isActive;
    public bool isCheck;

    public float fwd;
    public float up;
    public float right;

    private void Start()
    {
        isActive = false;
        isCheck = false;
    }


    // Update is called once per frame
    void Update()
    {
        if (!isActive)
        {
            return;
        }

        Vector3 toTarget = target.position - transform.position;
        
        up = Vector3.Dot(transform.up.normalized, toTarget.normalized);
        right = Vector3.Dot(transform.right.normalized, toTarget.normalized);
        fwd = Vector3.Dot(transform.forward.normalized, toTarget.normalized);

        if (fwd > 0.9f && up > -.2f && up < .2f && right > -.2f && right < .2f)
        {
            isCheck = true;
        }
    }
}
