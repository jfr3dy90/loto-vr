using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwichersController : MonoBehaviour
{
   public Switch[] switchers;
   
   public void SetSwitch(int index)
   {
      for (int i = 0; i < switchers.Length; i++)
      {
         switchers[i].col.enabled = index == i;
      }
   }
}
