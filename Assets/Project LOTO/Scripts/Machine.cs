using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Machine : MonoBehaviour
{
   
   public bool isStop;
   public bool isConected;

   private void Awake()
   {
      ActionsController.OnShotDownMachine += OnSetMachine;
      ActionsController.OnSetEnergySource += OnSetEnergySource;
   }

   private void OnDestroy()
   {
      ActionsController.OnShotDownMachine -= OnSetMachine;
      ActionsController.OnSetEnergySource -= OnSetEnergySource;
   }

   public void OnSetMachine(bool val)
   {
      isStop = val;
   }

   public void OnSetEnergySource(bool val)
   {
      isConected = val;
   }
}
