using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ActionsController
{
   //Evento creado para manejar la composicion de la ui
   public static Action <int, int, string, string, string, bool, int, Vector2> OnSetUi;
   
   //Evento creado para manejar el indicador de la ui
   public static Action <Vector3, Vector3,string, bool> OnSetIndicator;
   
   //Evento creado para cambiar el estado de la maquina
   public static Action <bool> OnShotDownMachine;

   //Evento creado para controlar posicion y rotacion del player
   public static Action<int> OnSetPlayerTr;
   
   //Evento creado para controlar posicion y rotacion del canvas
   public static Action<int> OnSetCanvasTr;
   
   //Evento para aislar fuente de energia
   public static Action<bool> OnSetEnergySource;
}
