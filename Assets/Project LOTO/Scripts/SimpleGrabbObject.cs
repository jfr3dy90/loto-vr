using System;
using UnityEngine;
using UnityEngine.Serialization;

public class SimpleGrabbObject : MonoBehaviour
{
    public int indexVrSocket;
    public bool xrSocket = true;
    private Transform tr;
    private Vector3 trPosition;
    private Quaternion trRotation;
    private Rigidbody rb;
    public SFXType dropSfx;
    public SFXType grabSfx;

    private void Awake()
    {
        tr = transform;
        trPosition = tr.position;
        trRotation = tr.rotation;
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            Invoke("BackToVrSocket", 3f);
            AudioManager.Instance.PlaySFX(transform.position, dropSfx);
        }
    }

    public void GrabSfx()
    {
        AudioManager.Instance.PlaySFX(transform.position, grabSfx);
    }

    public void UpdateVrSocket(int targetSocket)
    {
        indexVrSocket = targetSocket;
    }

    public void BackToVrSocket()
    {
        if (xrSocket)
        {
            GrabbableController.Instance.SetXrSockets(indexVrSocket);
        }
        else
        {
            BackPosition(); 
        }
    }

    void BackPosition()
    {
        rb.velocity = Vector3.zero;
        tr.position = trPosition;
        tr.rotation = trRotation; 
    }
}
 