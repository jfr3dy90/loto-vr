using System;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RefPositions))]
public class RefPositionsEditor : Editor
{
    private RefPositions refPositions;

    private void OnEnable()
    {
        refPositions = target as RefPositions;
    }

    private void OnSceneGUI()
    {
        for (int i = 0; i < refPositions.playerPositions.Length; i++)
        {
            refPositions.playerPositions[i].desirePos = Handles.DoPositionHandle(refPositions.playerPositions[i].desirePos, Quaternion.Euler(refPositions.playerPositions[i].desireRot));
        }

        for (int i = 0; i < refPositions.canvasPositions.Length; i++)
        {
            refPositions.canvasPositions[i].desirePos = Handles.DoPositionHandle(refPositions.canvasPositions[i].desirePos, Quaternion.Euler(refPositions.canvasPositions[i].desireRot));
        }
    }
    
    
}
