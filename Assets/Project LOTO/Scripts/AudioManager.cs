using UnityEngine;
using UnityEngine.Rendering.UI;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;
    public AudioSource m_sfx;
    public AudioSource m_machineLoop;

    [SerializeField] private AudioClip[] grabTool;
    [SerializeField] private AudioClip[] dropTool; 
    [SerializeField] private AudioClip[] grabItem;
    [SerializeField] private AudioClip[] CableDrop;
    
    [SerializeField] private AudioClip dropCover;
    [SerializeField] private AudioClip dropLock;

    [SerializeField] private AudioClip machineEnd;
    [SerializeField] private AudioClip machineSwitch;
    [SerializeField] private AudioClip ui;
    [SerializeField] private AudioClip useCover;
    [SerializeField] private AudioClip useLock;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void PlaySFX(Vector3 tr, SFXType sfx)
    {
        m_sfx.transform.position = tr;
        m_sfx.PlayOneShot(GetRandomAudio(sfx));
    }

    AudioClip GetRandomAudio(SFXType sfx)
    {
        AudioClip clip = null;
        switch (sfx)
        {
            case SFXType.Machine:
                clip = machineEnd;
                m_machineLoop.Stop();
                break;
            case SFXType.GrabTool:
                clip = GetRandomAudio(grabTool);
                break;
            case SFXType.DropTool:
                clip = GetRandomAudio(dropTool);
                break;
            case SFXType.GrabItem:
                clip = GetRandomAudio(grabItem);
                break;
            case SFXType.DropCover:
                clip = dropCover;
                break;
            case SFXType.DropLock:
                clip = dropLock;
                break;
            case SFXType.UI:
                clip = ui;
                break;
            case SFXType.Switch:
                clip = machineSwitch;
                break;
            case SFXType.UseCover:
                clip = useCover;
                break;
            case SFXType.UseLock:
                clip = useLock;
                break;
            case SFXType.Cable:
                clip = GetRandomAudio(CableDrop);
                break;
        }
        return clip;
    }

    AudioClip GetRandomAudio(AudioClip[] sfx)
    {
        AudioClip clip = sfx[Random.Range(0, sfx.Length)];
        return clip;
    }
}

public enum SFXType
{
    Machine,
    Switch,
    GrabTool,
    DropTool,
    GrabItem,
    DropCover,
    DropLock,
    UseCover,
    UseLock,
    Cable,
    UI
}
