using UnityEngine;
using UnityEngine.Events;

public class Switch : MonoBehaviour
{
    public bool isOn = false;
    public float rotation;
    public Collider col;
    public UnityEvent onSwitch;

    private void Awake()
    {
        col = GetComponent<Collider>();
        isOn = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Hand")
        {
            isOn = false;
            onSwitch?.Invoke();
            AudioManager.Instance.PlaySFX(transform.position, SFXType.Switch);
        }
    }
}