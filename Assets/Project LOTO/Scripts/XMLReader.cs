using System.IO;
using UnityEngine;
using System.Xml;

public class XMLReader : MonoBehaviour
{
    [SerializeField] private string folderPath;
    private TextAsset xmlRawFile;
    private XmlDocument xmlDoc;

    private void Start()
    {
        xmlRawFile = Resources.Load<TextAsset>(folderPath + XperienceManager.Instance.actualAnguaje);
        string xmlData = xmlRawFile.text;
        xmlDoc = new XmlDocument();
        xmlDoc.Load(new StringReader(xmlData));
    }

    public string GetNodeText(string xPath)
    {
        XmlNode node = xmlDoc.SelectSingleNode(xPath);
        string comments = node.InnerText;
        return comments;
    }


}