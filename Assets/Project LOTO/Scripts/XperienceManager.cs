using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;


public class XperienceManager : MonoBehaviour
{
    [SerializeField] private XMLReader reader;

    public static XperienceManager Instance;
    public Languaje actualAnguaje;
    public Step actualStep;

    private IEnumerator actionsStep;
    [SerializeField] private SwichersController switchs;
    //[SerializeField] private PhysicsButton btnStop;
    [SerializeField] private Machine machine;
    [SerializeField] private HeadController headController;
    [Range(1f, 3.5f)] public float timeWait;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        machine.isStop = false;
        SetXperienceStep(actualStep);
        GrabbableController.Instance.SetGrabbable(-1);
        GrabbableController.Instance.SetToolBox(false);
        Arrow.Instance.SetArrow(false);
        // grabbables.coll.enabled = false;
    }

    public void SetXperienceStep(Step _actualStep)
    {
        GrabbableController.Instance.SetGrabbable(-1);
        switch (_actualStep)
        {
            case Step.Welcome:
                ActionsController.OnSetCanvasTr?.Invoke(0);
                ActionsController.OnSetPlayerTr?.Invoke(0);
                ActionsController.OnSetUi?.Invoke(1, 0, reader.GetNodeText("/Steps/Welcome/Title"), reader.GetNodeText("/Steps/Welcome/Msj"), reader.GetNodeText("/Steps/Welcome/BtnText"), true, 0, new Vector2(80,20));
                break;
            case Step.Step_1:
                ActionsController.OnSetUi?.Invoke(1, 0, reader.GetNodeText("/Steps/Step_1/Title"), reader.GetNodeText("/Steps/Step_1/Msj"), reader.GetNodeText("/Steps/Step_1/BtnText"), true, 1,new Vector2(80,20));
                break;
            case Step.Step_2:
                ActionsController.OnSetUi?.Invoke(1, 0, reader.GetNodeText("/Steps/Step_2/Title"), reader.GetNodeText("/Steps/Step_2/Msj"), reader.GetNodeText("/Steps/Step_2/BtnText"), true, 2, new Vector2(80,20));
                break;
            case Step.Step_3:
                ActionsController.OnSetUi?.Invoke(1, 0, reader.GetNodeText("/Steps/Step_3/Title"), reader.GetNodeText("/Steps/Step_3/Msj"), reader.GetNodeText("/Steps/Step_3/BtnText"), true, 3, new Vector2(90,20));
                break;
            case Step.ApagadoMaquina :
                actionsStep = Step_3_Actions();
                OnStartActionSteps();
                break;
            case Step.Step_4 :
                ActionsController.OnSetUi?.Invoke(0,0,reader.GetNodeText("/Steps/Step_4/Title"), reader.GetNodeText("/Steps/Step_4/Msj"), null, false, 0, new Vector2(90,33));
                actionsStep = Step_4_Actions();
                OnStartActionSteps();
                break;
            case Step.Step_5 :
                ActionsController.OnSetCanvasTr?.Invoke(0);
                ActionsController.OnSetPlayerTr?.Invoke(0);
                ActionsController.OnSetUi?.Invoke(1,0,reader.GetNodeText("/Steps/Step_5/Title"), reader.GetNodeText("/Steps/Step_5/Msj"), reader.GetNodeText("/Steps/Step_5/BtnText"), true, 4, new Vector2(96,33));
                break;
            case Step.ApplicateLock:
                actionsStep = Step_5_Actions();
                OnStartActionSteps();
                break;
            case Step.Step_6:
                ActionsController.OnSetCanvasTr?.Invoke(0);
                ActionsController.OnSetPlayerTr?.Invoke(0);
                ActionsController.OnSetUi?.Invoke(1,0,reader.GetNodeText("/Steps/Step_6/Title"), reader.GetNodeText("/Steps/Step_6/Msj"), reader.GetNodeText("/Steps/Step_6/BtnText"), true, 5, new Vector2(80,20));
                break;
            case Step.CheckEnergy:
                ActionsController.OnSetUi?.Invoke(0,0,reader.GetNodeText("/Steps/Step_6/Title"), reader.GetNodeText("/Steps/Step_6/IndicationOne"), null, false, 0, new Vector2(80,20));
                actionsStep = Step_6_Actions();
                OnStartActionSteps();
                break;
            case Step.Step_7:
                ActionsController.OnSetCanvasTr?.Invoke(0);
                ActionsController.OnSetPlayerTr?.Invoke(0);
                ActionsController.OnSetUi?.Invoke(1,0,reader.GetNodeText("/Steps/Step_7/Title"), reader.GetNodeText("/Steps/Step_7/Msj"), reader.GetNodeText("/Steps/Step_7/BtnText"), true, 6, new Vector2(90,33));
                break;
            case Step.StartMachine:
                actionsStep = Step_7_Actions();
                OnStartActionSteps();
                break;
            case Step.Finish:
                ActionsController.OnSetUi?.Invoke(1,0,reader.GetNodeText("/Steps/Finish/Title"), reader.GetNodeText("/Steps/Finish/Msj"), reader.GetNodeText("/Steps/Finish/BtnText"), true, 7, new Vector2(90,33));
                break;
        }

        actualStep = _actualStep;
    }

    private IEnumerator Step_3_Actions()
    {
        ActionsController.OnSetCanvasTr?.Invoke(1);
        ActionsController.OnSetPlayerTr?.Invoke(1);
        ActionsController.OnSetUi?.Invoke(0, 0, reader.GetNodeText("/Steps/Step_3/Title"), reader.GetNodeText("/Steps/Step_3/Msj"), null, false, 0, new Vector2(90,20));
        
        Arrow.Instance.SetArrow(true);
        for (int i = 0; i < switchs.switchers.Length; i++)
        {
            switchs.SetSwitch(i);
            ActionsController.OnSetIndicator?.Invoke(switchs.switchers[i].transform.position + Vector3.up * 0.10f, new Vector3(0f,0f,0f), (i + 1).ToString(), true);
            Arrow.Instance.OnStartMovement(switchs.switchers[i].transform.position + switchs.switchers[i].transform.TransformDirection(Vector3.forward * 0.4f), switchs.switchers[i].transform.position, new Vector3(0f,0f,0.2f),0);
            while (switchs.switchers[i].isOn)
            {
                yield return new WaitForEndOfFrame();
            }
        }

        while (!machine.isStop)
        {
            yield return new WaitForEndOfFrame();
        }
        switchs.SetSwitch(-1);
        Arrow.Instance.SetArrow(false);
        AudioManager.Instance.PlaySFX(AudioManager.Instance.m_machineLoop.transform.position, SFXType.Machine);
        yield return new WaitForSeconds(timeWait);
        
        ActionsController.OnSetIndicator?.Invoke(Vector3.zero, Vector3.zero, "0", false);
        SetXperienceStep(Step.Step_4);
    }

    private IEnumerator Step_4_Actions()
    {
        ActionsController.OnSetCanvasTr?.Invoke(2);
        ActionsController.OnSetPlayerTr?.Invoke(2);
        GrabbableController.Instance.coll.enabled = true;
        
        Arrow.Instance.SetArrow(true);
        Arrow.Instance.OnStartMovement(
            GrabbableController.Instance.m_Socket[4].transform.position + GrabbableController.Instance.m_Socket[4].transform.TransformDirection(Vector3.up * .4f),
            GrabbableController.Instance.m_Socket[4].transform.position,
            new Vector3(0f,0.2f, 0), 0);
        while (machine.isConected)
        {
            yield return new WaitForEndOfFrame();
        }
        Arrow.Instance.SetArrow(false);
        
        GrabbableController.Instance.m_Socket[4].socketActive = false;
        
        while (!GrabbableController.Instance.coll.GetComponent<SimpleVRState>().actualState)
        {
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(timeWait);
        
        SetXperienceStep(Step.Step_5);
    }

    private IEnumerator Step_5_Actions()
    {
        ActionsController.OnSetUi?.Invoke(0,0,reader.GetNodeText("/Steps/Step_5/Title"), reader.GetNodeText("/Steps/Step_5/Msj"), null, true, 0, new Vector2(96,33));
        ActionsController.OnSetCanvasTr?.Invoke(1);
        ActionsController.OnSetPlayerTr?.Invoke(1);
        GrabbableController.Instance.SetToolBox(true);
        GrabbableController.Instance.SetGrabbable(0);
        GrabbableController.Instance.SetXrSockets(5);
        GrabbableController.Instance.SetXrSockets(6);


        Arrow.Instance.SetArrow(true);
        Arrow.Instance.OnStartMovement(
            GrabbableController.Instance.m_grabbables[2].transform.position + GrabbableController.Instance.m_grabbables[2].transform.TransformDirection(Vector3.up * .4f),
            GrabbableController.Instance.m_grabbables[2].transform.position,
            new Vector3(0f,0.2f, 0), 0);
        while (!GrabbableController.Instance.m_Socket[0].GetComponentInChildren<SimpleVRState>().actualState)
        {
            yield return new WaitForEndOfFrame();
        }
        
        GrabbableController.Instance.m_grabbables[0].GetComponent<SimpleGrabbObject>().UpdateVrSocket(0);
        GrabbableController.Instance.SetGrabbable(1);
       
        while (!GrabbableController.Instance.m_Socket[1].GetComponentInChildren<SimpleVRState>().actualState)
        {
            yield return new WaitForEndOfFrame();
        }
        
        GrabbableController.Instance.m_grabbables[1].GetComponent<SimpleGrabbObject>().UpdateVrSocket(1);
        GrabbableController.Instance.SetToolBox(false);
        GrabbableController.Instance.SetGrabbable(-1);
        Arrow.Instance.SetArrow(false);
        yield return new WaitForSeconds(timeWait);

        SetXperienceStep(Step.Step_6);
    }

    private IEnumerator Step_6_Actions()
    {
        GrabbableController.Instance.SetGrabbable(-1);
        GrabbableController.Instance.SetXrSockets(0);
        GrabbableController.Instance.SetXrSockets(1);
        
        GrabbableController.Instance.SetToolBox(false);
        ActionsController.OnSetCanvasTr?.Invoke(2);
        ActionsController.OnSetPlayerTr?.Invoke(2);
        
        headController.isActive = true;
        
        Arrow.Instance.SetArrow(true);
        Arrow.Instance.OnStartMovement(
            GrabbableController.Instance.m_Socket[4].transform.position + GrabbableController.Instance.m_Socket[4].transform.TransformDirection(Vector3.up * .4f),
            GrabbableController.Instance.m_Socket[4].transform.position,
            new Vector3(0f,0.2f, 0), 0);
        
        while (!headController.isCheck)
        {
            yield return  new WaitForEndOfFrame();
        }
        AudioManager.Instance.PlaySFX(new Vector3(3, .4f, 3.3f), SFXType.UI);

        Arrow.Instance.SetArrow(false);
        headController.isActive = false;
        yield return new WaitForSeconds(timeWait);
        
        ActionsController.OnSetUi?.Invoke(0,0,reader.GetNodeText("/Steps/Step_6/Title"), reader.GetNodeText("/Steps/Step_6/IndicationTwo"), null, false, 0, new Vector2(80,20));

        ActionsController.OnSetCanvasTr?.Invoke(1);
        ActionsController.OnSetPlayerTr?.Invoke(1);
       
        
        Arrow.Instance.SetArrow(true);
        Arrow.Instance.OnStartMovement(
            GrabbableController.Instance.m_grabbables[2].transform.position + GrabbableController.Instance.m_grabbables[2].transform.TransformDirection(Vector3.up * .4f),
            GrabbableController.Instance.m_grabbables[2].transform.position,
            new Vector3(0f,0.2f, 0), 0);
        GrabbableController.Instance.m_grabbables[0].GetComponent<BoxCollider>().enabled = false;
        GrabbableController.Instance.m_grabbables[1].GetComponent<BoxCollider>().enabled = false;
        
        GrabbableController.Instance.SetGrabbable(2);
        while (GrabbableController.Instance.m_grabbables[2].gameObject.activeSelf)
        {
            yield return new WaitForEndOfFrame();
        }AudioManager.Instance.PlaySFX(new Vector3(.29f, .55f,3.3f), SFXType.UI);
        Arrow.Instance.SetArrow(false);

        yield return new WaitForSeconds(timeWait);
        SetXperienceStep(Step.Step_7);
    }

    private IEnumerator Step_7_Actions()
    {        
        GrabbableController.Instance.SetGrabbable(-1);
        GrabbableController.Instance.SetXrSockets(0);
        GrabbableController.Instance.SetXrSockets(1);
        
        ActionsController.OnSetCanvasTr(3);
        ActionsController.OnSetPlayerTr(3);
        ActionsController.OnSetUi?.Invoke(0,0,reader.GetNodeText("/Steps/Step_7/Title"), reader.GetNodeText("/Steps/Step_7/IndicationOne"), null, false, 0, new Vector2(90,33));

        GrabbableController.Instance.SetToolBox(true);
        GrabbableController.Instance.SetGrabbable(2);
        
        Arrow.Instance.SetArrow(true);
        Arrow.Instance.OnStartMovement(
            GrabbableController.Instance.m_grabbables[4].transform.position + GrabbableController.Instance.m_grabbables[4].transform.TransformDirection(Vector3.forward * -.4f),
            GrabbableController.Instance.m_grabbables[4].transform.position,
            new Vector3(0f,0.2f, 0), 0);
        
        SimpleVRState vrState = GrabbableController.Instance.m_Socket[2].GetComponent<SimpleVRState>();
        GrabbableController.Instance.m_Socket[2].socketActive = true;
        while(!vrState.actualState )
        {
            yield return new WaitForEndOfFrame();
        }

        GrabbableController.Instance.m_grabbables[4].GetComponent<SimpleGrabbObject>().xrSocket = true;
        Arrow.Instance.SetArrow(false);        
        
        yield return new WaitForSeconds(timeWait);
        
        ActionsController.OnSetCanvasTr(4);
        ActionsController.OnSetPlayerTr(4);
        
        
        Arrow.Instance.SetArrow(true);
        Arrow.Instance.OnStartMovement(
            GrabbableController.Instance.m_grabbables[3].transform.position + GrabbableController.Instance.m_grabbables[3].transform.TransformDirection(Vector3.up * .4f),
            GrabbableController.Instance.m_grabbables[3].transform.position,
            new Vector3(0f,0.2f, 0), 0);
        
        GrabbableController.Instance.SetGrabbable(3);
        vrState = GrabbableController.Instance.m_Socket[3].GetComponent<SimpleVRState>();
        GrabbableController.Instance.m_Socket[3].socketActive = true;
        while (!vrState.actualState)
        {
            yield return new WaitForEndOfFrame();
        }

        Arrow.Instance.SetArrow(false);
        GrabbableController.Instance.m_grabbables[3].GetComponent<SimpleGrabbObject>().xrSocket = true;
        GrabbableController.Instance.SetGrabbable(-1);
        yield return new WaitForSeconds(timeWait);
        
        ActionsController.OnSetUi?.Invoke(0,0,reader.GetNodeText("/Steps/Step_7/Title"), reader.GetNodeText("/Steps/Step_7/IndicationTwo"), reader.GetNodeText("/Steps/Step_7/BtnTextTwo"), true, 7, new Vector2(90,33));
        ActionsController.OnSetCanvasTr(0);
        ActionsController.OnSetPlayerTr(1);

        Arrow.Instance.SetArrow(true);
        Arrow.Instance.OnStartMovement(
            GrabbableController.Instance.m_grabbables[2].transform.position + GrabbableController.Instance.m_grabbables[2].transform.TransformDirection(Vector3.up * .4f),
            GrabbableController.Instance.m_grabbables[2].transform.position,
            new Vector3(0f,0.2f, 0), 0);
        
        GrabbableController.Instance.SetGrabbable(1);
        vrState = GrabbableController.Instance.m_Socket[6].GetComponent<SimpleVRState>();
        while (!vrState.actualState)
        {
            yield return new WaitForEndOfFrame();
        }
        
        GrabbableController.Instance.SetGrabbable(0);
        vrState = GrabbableController.Instance.m_Socket[5].GetComponent<SimpleVRState>();
        while (!vrState.actualState)
        {
            yield return new WaitForEndOfFrame();
        }
        Arrow.Instance.SetArrow(false);
        
        
        yield return new WaitForSeconds(timeWait);
        GrabbableController.Instance.DidableGrababbles();
        GrabbableController.Instance.SetToolBox(false);
       
       
        ActionsController.OnSetUi?.Invoke(1,0,reader.GetNodeText("/Steps/Step_7/Title"), reader.GetNodeText("/Steps/Step_7/IndicationTree"), reader.GetNodeText("/Steps/Step_7/BtnTextTwo"), true, 7, new Vector2(90,33));
        yield return new WaitForEndOfFrame();
        SetXperienceStep(Step.Finish);
    }

    void OnStartActionSteps()
    {
        if (actionsStep != null)
        {
            StopCoroutine(actionsStep);
        }
        StartCoroutine(actionsStep);
    }


    public void LoadSim()
    {
        SceneManager.LoadScene(0);
    }
}

public enum Step
{
    Welcome,
    Step_1,
    Step_2,
    Step_3,
    ApagadoMaquina,
    Step_4,
    Step_5,
    ApplicateLock,
    Step_6,
    CheckEnergy,
    Step_7,
    StartMachine,
    Finish
}

public enum Languaje
{
    Spanish,
    English
}