using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefPositions : MonoBehaviour
{
   public DesirePosAndRot[] playerPositions;
   [SerializeField] private Transform player;

   public DesirePosAndRot[] canvasPositions;
   [SerializeField] private Transform canvas;
   [SerializeField] private float speed;
   private IEnumerator moveCanvas;
   [SerializeField] private bool isAnimated;
   
   private void Awake()
   {
      ActionsController.OnSetPlayerTr += SetPlayerPosition;
      ActionsController.OnSetCanvasTr += SetCanvasPosition;
   }

   private void OnDestroy()
   {
      ActionsController.OnSetPlayerTr -= SetPlayerPosition;
      ActionsController.OnSetCanvasTr -= SetCanvasPosition;
   }

   public void SetPlayerPosition(int index)
   {
      player.transform.position = playerPositions[index].desirePos;
      player.transform.rotation = Quaternion.Euler(playerPositions[index].desireRot);

      ResetHands();
   }

   public void SetCanvasPosition(int index)
   {
      if (!isAnimated)
      {
         canvas.transform.position = canvasPositions[index].desirePos;
         canvas.transform.rotation = Quaternion.Euler(canvasPositions[index].desireRot);
      }
      else
      {
         OnMoveCanvas(index);
      }
   }

   void OnMoveCanvas(int index)
   {
      if (moveCanvas != null)
      {
         StopCoroutine(moveCanvas);
      }

      moveCanvas = MoveCanvas(index);
      StartCoroutine(moveCanvas);
   }

   IEnumerator MoveCanvas(int index)
   {
      float a = 0;
      float b = 1;
      
      
      while (a < b)
      {
         yield return new WaitForEndOfFrame();
         canvas.position = Vector3.Lerp(canvas.position, canvasPositions[index].desirePos, a);
         canvas.rotation = Quaternion.Lerp(canvas.rotation, Quaternion.Euler(canvasPositions[index].desireRot), a);
         a += Time.deltaTime;
      }
   }

   public void ResetHands()
   {
      //leftHand.ResetHand();
      //rightHand.ResetHand();
   }
}

[Serializable]
public class DesirePosAndRot
{
   public Vector3 desirePos;
   public Vector3 desireRot;
}


