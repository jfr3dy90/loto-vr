using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAudio : MonoBehaviour
{
    [SerializeField] private SFXType _sfxType;
    [SerializeField] private SFXType _grabSfxType;
    public bool extra = false;

    public void PlaySfx()
    {
        AudioManager.Instance.PlaySFX(transform.position, _sfxType);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (extra)
        {
            AudioManager.Instance.PlaySFX(transform.position, _sfxType);
        }
    }

    public void PlayGrab()
    {
        AudioManager.Instance.PlaySFX(transform.position, _grabSfxType);
    }
}
