using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(Animator))]
public class Hand : MonoBehaviour
{
    [Header("Animation")] 
    private Animator animator;

    [SerializeField] private ActionBasedController baseController;

    [Range(0f,1f)]
    public float gripTarget;
    private float gripCurrent;
    private string animatorGrip = "Close";

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        gripTarget = baseController.selectAction.action.ReadValue<float>();
        AnimateHand();
    }

    void AnimateHand()
    {
        if (gripCurrent != gripTarget)
        {
            gripCurrent = gripTarget;
            animator.SetFloat(animatorGrip, gripCurrent);
        }
    }
}